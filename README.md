<!-- SPDX-License-Identifier: Apache-2.0 -->
<!-- SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen> -->

# Villager Discount Stacking

Adds [MC-181190] back into the game with just a datapack.

[MC-181190]: https://bugs.mojang.com/browse/MC-181190

## Build

Run the following:

``` sh
poetry install --no-root
poetry run beet
```

## Copyright

Copyright &copy; 2024 [nfitzen](https://gitlab.com/nfitzen).

Licensed under [Apache 2.0](LICENSE).

## Resources

If you're viewing this file in object code form (i.e. as a datapack
ZIP), you can find the source code at
<https://gitlab.com/nfitzen/villager-discount>.
