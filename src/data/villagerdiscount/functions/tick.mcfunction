# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen>

execute as @e[type=minecraft:villager,tag=!villagerdiscount.post_cure,predicate=villagerdiscount:cured] \
    run function ./post_cure
