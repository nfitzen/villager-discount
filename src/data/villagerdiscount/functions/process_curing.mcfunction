# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen>

# executed as zombie villager

data modify storage villagerdiscount:input player set from entity @s ConversionPlayer

data merge storage villagerdiscount:input {"type": "major_positive", "min": 0, "max": 100, "delta": 20}
function ./bump_gossip with storage villagerdiscount:input
data merge storage villagerdiscount:input {"type": "minor_positive", "min": 0, "max": 200, "delta": 25}
function ./bump_gossip with storage villagerdiscount:input

tag @s add villagerdiscount.curing_processed
