# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen>

execute unless data entity @s Offers.Recipes[-1].buy.tag._villagerdiscount \
    run \
data modify entity @s Offers.Recipes append value { \
    "buy": { \
        "id":"minecraft:barrier", \
        "Count":1b, \
        "tag": { \
            "_villagerdiscount": {"exists":true} \
        } \
    }, \
    "sell": { \
        "id": "minecraft:barrier", "Count": 1b \
    }, \
    "maxUses": 0, \
}

data modify entity @s Offers.Recipes[-1].buy.tag._villagerdiscount.player \
    set from entity @s ConversionPlayer
