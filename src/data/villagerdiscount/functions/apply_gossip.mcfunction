# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen>

# applies gossip copied after curing
# villagerdiscount:input player must exist

# say hi
# $say curing $(type)
# $say quotes: "$(type)"

data remove storage villagerdiscount:input value
$data modify storage villagerdiscount:input value set from entity @s Offers.Recipes[-1].buy.tag._villagerdiscount.$(type)
$data modify storage villagerdiscount:input type set value "$(type)"

execute if data storage villagerdiscount:input value run function ./set_gossip_from_input with storage villagerdiscount:input:
    $data modify entity @s Gossips[{"Type":"$(type)","Target":$(player)}].Value \
        set value $(value)
