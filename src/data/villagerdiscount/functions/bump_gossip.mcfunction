# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen>

$execute store result score $value villagerdiscount.int \
    run data get entity @s Gossips[{"Type":"$(type)","Target":$(player)}].Value

$execute if score $value villagerdiscount.int matches ..$(min) run return fail

function ./create_dummy_trade

$scoreboard players add $value villagerdiscount.int $(delta)
$execute if score $value villagerdiscount.int matches $(max).. \
    run scoreboard players set $value villagerdiscount.int $(max)

$execute store result entity @s \
    Offers.Recipes[-1].buy.tag._villagerdiscount.$(type) int 1 \
    run scoreboard players get $value villagerdiscount.int
