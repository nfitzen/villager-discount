# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen>

execute as @e[type=minecraft:zombie_villager,tag=!villagerdiscount.curing_processed] \
    if data entity @s ConversionPlayer \
    run function ./process_curing

schedule function ./second 1s replace
