# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2024 nfitzen <https://gitlab.com/nfitzen>

tag @s add villagerdiscount.post_cure

execute unless data entity @s Offers.Recipes[-1].buy.tag._villagerdiscount run return fail

data remove storage villagerdiscount:input player
data modify storage villagerdiscount:input player \
    set from entity @s Offers.Recipes[-1].buy.tag._villagerdiscount.player

function ./apply_gossip {"type": "major_positive"}
function ./apply_gossip {"type": "minor_positive"}

data remove entity @s Offers.Recipes[{"buy":{"tag":{"_villagerdiscount":{"exists":true}}}}]
execute unless data entity @s Offers.Recipes[] run data remove entity @s Offers
